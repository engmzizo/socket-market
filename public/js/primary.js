var socket;
var user = {};

jQuery( window ).load(function() {
	jQuery('.sign-in').click(function(e){
		e.preventDefault();
		socket = io.connect('http://127.0.0.1:3000',{'sync disconnect on unload': true});
		var username = jQuery('#username').val();
		socket.on('username_required', function (data) {
			unbind_all(socket);
			// remove all listeners
			// 
			socket.emit('username',{username:username});
		});
		socket.on('admin_loggedin',function(){
			jQuery.get('/admin',{},function(data){
				jQuery('.container').html(data);
				admin_hooks(socket);
			});
		});
		socket.on('loggedin',function(){
			jQuery.get('/account/'+username,{},function(data){
				jQuery('.container').html(data);
				user_hooks(socket);
			});
		});
	});
});


function admin_hooks(socket){
	jQuery('.insert-product').click(function(e){
		e.preventDefault();
		socket.emit('insert_product',{
			product_name:jQuery('#product_name').val(),
			product_quantity:jQuery('#product_quantity').val(),
			product_price:jQuery('#product_price').val()});
	});
	socket.on('product_added',function(data){
		jQuery('.products-table').append('<tr id='+data._id+'><td>'+data.name+'</td><td>'+data.quantity+'</td><td>$'+data.price+'</td></tr>');
	});
	socket.on('product_updated',function(data){
		jQuery('.products-table #'+data._id).html('<td>'+data.name+'</td><td>'+data.quantity+'</td><td>$'+data.price+'</td>');
	});
}

function user_hooks(socket){
	user_listener(socket);
	build_chart();
	socket.on('product_added',function(data){
		jQuery('.products-table').append(jQuery('<tr class="'+data._id+'"><td id="'+data._id+'-name">'+data.name+'</td><td id="'+data._id+'-quantity">'+data.quantity+'</td><td id="'+data._id+'-price">$'+data.price+'</td><td><a href="#" class="btn btn-sm btn-primary buy-product"  data-price="'+data.price+'" data-quantity="'+data.quantity+'" data-name="'+data.name+'" data-product_id="'+data._id+'">Buy</a></td></tr>'));
		user_listener(socket,'.'+data._id+' .buy-product');
	});

	socket.on('product_updated',function(data){
		jQuery('#'+data._id+'-name').text(data.name);
		jQuery('#'+data._id+'-quantity').text(data.quantity);
		jQuery('#'+data._id+'-price').text('$'+data.price);
		jQuery('.'+data._id+' .buy-product').attr({'data-name':data.name,'data-quantity':data.quantity,'data-price':data.price,'data-product_id':data._id});
	});

	socket.on('user_updated',function(data){
		jQuery('#cash').text(data.cash);
	});

	socket.on('user_product_updated',function(data){
		console.log(data);
		if( jQuery('#'+data.product_id).length)
			jQuery('#'+data.product_id).text(data.productName+ ' (' +data.quantity+')');
		else{
			jQuery('.user-products').append('<li id='+data.product_id+'>'+data.productName+ ' (' +data.quantity+')</li>')
		}
	});
	socket.on('product_status_updated',function(data){
		draw_chart(data);
	});
	setInterval(function(){
		socket.emit('get_product_status');
	},60000);
}

function user_listener(socket,select_query){
	select_query = select_query || '.buy-product';
	user._id = jQuery('#username').attr('data-user_id');
	user.username = jQuery('#username').text();
	user.cash = jQuery('#cash').text();
	jQuery(select_query).click(function(e){
		e.preventDefault();
		var product = jQuery(this);
		if(product.attr('data-quantity') == '0') return alert('Sorry, Quantity is 0 !');
		if(parseInt(user.cash) < parseInt(product.attr('data-price'))) return alert('Sorry, Not enough cash!');
		socket.emit('buy_product',{user_id:user._id,product_id:product.attr('data-product_id')});
	});
}

function draw_chart(d){
	if(d.length == 0) return ;
	var data = google.visualization.arrayToDataTable(d);
	var options = {
	  	title: 'Product purchase per minute',
	  	hAxis: {title: 'Last Hour'}
	};
	var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	chart.draw(data, options);
}

function build_chart(){
	var r = JSON.parse(jQuery('#chart_values').val());
	draw_chart(r);
}

function unbind_all(socket){
	var listeners = ['product_added','product_updated','user_updated','user_product_updated','product_status_updated'];
	for (var i = 0; i < listeners.length; i++) {
		socket.removeAllListeners(listeners[i]);
	}
	jQuery('.insert-product').unbind();
	jQuery('.buy-product').unbind();
}