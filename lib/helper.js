var model = require('./model');

module.exports.getProducts = function(fn){
    model.Products.find({},function(err,products){
        fn(products);
    });
}
module.exports.getUserProducts = function(user_id,fn){
    model.UserProducts.find({user_id:user_id},function(err,userProducts){
        fn(userProducts);
    });
}
module.exports.getStatus = function(fn){
    var products_query = model.Products.find({time:{ $gt: parseInt(Date.now()/60000) - 60 }});
    products_query.limit(5).sort('-time -purchased').exec(function(err,products){
        build_status(products,null,0,function(productStats){
            fn(build_chart_data(productStats));
        });
    });
}

module.exports.updateStatus = function(data,fn){
    var helper = this ;
    model.ProductStats.findOne({product_id: data.product_id,time: parseInt(Date.now()/60000)},function(err,productStats){
        if(productStats == null)
            productStats = new model.ProductStats({product_id: data.product_id,quantity: 1,time:parseInt(Date.now()/60000)});
        else
            ++productStats.quantity;
        productStats.save(function(err,productStats){
            setTimeout(function(){
                helper.getStatus(function(status){
                    fn(status);
                });
            },1000);
        });
    });
}
module.exports.fetchData = function(user,fn){
    var helper = this ;
    var data = {};
    data.user = user;
    helper.getProducts(function(products){
        data.products = products;
        data.productsNames = helper.productsNames(products);
        helper.getUserProducts(user._id,function(userProducts){
            data.userProducts = userProducts;
            helper.getStatus(function(productStats){
                data.stats = productStats;
                fn(data);
            });
        });
    });
}
module.exports.productsNames = function(products){
    var productsNames = {};
    if(products != null && products.length > 0){
        for (var i = 0; i < products.length; i++) {
            productsNames[products[i]._id] = products[i].name;
        }
    }
    return productsNames;
}
module.exports.getProductById = function(product_id,fn){
    model.Products.findById(product_id,function(err,product){
        fn(product);
    });
}
module.exports.getUserById = function(user_id,fn){
    model.Users.findById(user_id,function(err,user){
        fn(user);
    });
}
module.exports.getUser = function(userData,fn){
    var helper = this;
    model.Users.findOne(userData,function(err,user){
        if(user){
            helper.fetchData(user,function(data){
                fn(data);
            });
        }
        else{
            var user_doc = {username: userData.username,cash: 100};
            new model.Users(user_doc).save(function(err,user){
                helper.fetchData(user,function(data){
                    fn(data);
                });
            });
        }
    });
}
module.exports.addProduct = function(data,fn){
    var product = new model.Products({
        name: data.product_name,
        quantity: data.product_quantity,
        price: data.product_price
    });
    product.save(function(err,product){
        fn(product);
    });
}
module.exports.getUserProduct = function(user_id,product_id,fn){
    model.UserProducts.findOne({user_id:user_id,product_id:product_id},function(err,userProduct){
        if( userProduct == null )
            userProduct = new model.UserProducts({user_id: user_id,product_id: product_id,quantity: 0});
        fn(userProduct);
    });
}

function build_status(products,productStatsObj,i,fn){
    if(i == products.length)  return fn(productStatsObj);
    productStatsObj = productStatsObj || {};
    
    model.ProductStats.find({product_id:products[i]._id,time:{ $gt: parseInt(Date.now()/60000) - 60 }},function(err,productStats){
        if(productStats.length == 0) return build_status(products,productStatsObj,++i,fn);
        productStatsObj[products[i]._id] = {name:products[i].name,productStats:productStats,max:productStats[0]['quantity'],min:productStats[productStats.length-1]['quantity']};
        build_status(products,productStatsObj,++i,fn);
    });
}

function build_chart_data(productStats){
    if(isEmpty(productStats)) return [];
    var chart_data = [];// [['','one']]
    var products_names = [''];
    for(var productStat in productStats){
        products_names.push(productStats[productStat].name);
    }
    var status_holder = {};
    for(var productStat in productStats){
        status_holder[productStats[productStat].name] = {};
        for (var k = 0; k < productStats[productStat].productStats.length; k++) {
            status_holder[productStats[productStat].name][60 - (parseInt(Date.now()/60000) - productStats[productStat].productStats[k].time)] = productStats[productStat].productStats[k].quantity;
        }
    }
    chart_data.push(products_names);
        for (var i = 1; i < 61; i++) {
            var s = [''];
            for (var j = 1; j < chart_data[0].length; j++){
                if(status_holder[chart_data[0][j]][i] === undefined){
                    s.push(0);
                }
                else{
                    s.push(status_holder[chart_data[0][j]][i]);
                }
            }
            chart_data[i] = s;
        }
    return chart_data;
}

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
    return true;
}
