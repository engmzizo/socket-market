var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/marketdb');
var db = mongoose.connection;
var productSchema = mongoose.Schema({
    name: String,
    quantity: Number,
    price: Number,
    purchased: { type : Number, default: 0 },
    time : { type : Number, default: 0 }
});
var usersSchema = mongoose.Schema({
    username: String,
    cash: Number
});
var userProductsSchema = mongoose.Schema({
    user_id: String,
    product_id: String,
    quantity: Number
});
var productStatsSchema = mongoose.Schema({
    product_id: String,
    quantity: Number,
    time : Number
});

module.exports.Products = mongoose.model('Products', productSchema);
module.exports.Users = mongoose.model('Users', usersSchema);
module.exports.UserProducts = mongoose.model('UserProducts', userProductsSchema);
module.exports.ProductStats = mongoose.model('ProductStats', productStatsSchema);
