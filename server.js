var helper = require('./lib/helper');
var online_users = {};
var io;

module.exports = function(server){
	io = require('socket.io')(server);
	runServer();
}

var runServer = function() {
	io.on('connection', function (socket) {
	    socket.emit('username_required');
	    socket.on('username', function (usernameData) {
	    	socket.username = usernameData.username;
	    	online_users[usernameData.username] = socket;
			if(usernameData.username == 'admin'){
	    	 	socket.emit('admin_loggedin');
	    	}
	    	else{
	    		socket.emit('loggedin');
	    	}
	    });
		socket.on('disconnect', function(){
			delete online_users[socket.username];
		});
		socket.on('insert_product', function(data){
			helper.addProduct(data,function(product){
	    		io.emit('product_added',product);
			});
		});
		socket.on('get_product_status', function(){
			helper.getStatus(function(status){
	    		io.emit('product_status_updated',status);
			});
		});
		socket.on('buy_product', function(data){
			helper.getProductById(data.product_id,function(product){
				if(product.quantity == 0) return ;
				helper.getUserById(data.user_id,function(user){
					if(user.cash < product.price) return ;
					user.cash -= product.price;
					--product.quantity;
					++product.purchased;
					product.time = parseInt(Date.now()/60000);
					user.save();
					product.save();
					helper.getUserProduct(data.user_id,data.product_id,function(userProduct){
							++userProduct.quantity;
						userProduct.save(function(err,userProduct){
							var userProductData = {productName:product.name,quantity:userProduct.quantity,product_id:userProduct.product_id};
							socket.emit('user_product_updated',userProductData);
						});
						helper.updateStatus(data,function(status){
							io.emit('product_status_updated',status);
						});
					});
					io.emit('product_updated',product);
					socket.emit('user_updated',user);
				});	
			});
		});
	});
}